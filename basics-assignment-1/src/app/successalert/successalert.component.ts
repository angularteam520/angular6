import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-successalert',
    templateUrl: './successalert.component.html',
    // template: `app-successalert`,  // Could not get result! Why it is wrong?
    // templateUrl: `../warningalert/warningalert.component.html`,  // It's fine.
    styles: [`
        p {
            color: green;
        }
    `]
})
export class SuccessalertComponent implements OnInit {

    constructor() { }
    
    ngOnInit() {
    }
  
}