import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  username = '';
  allowclick = false;

  ngOnInit() {
  }

  onUpdateUser(event: Event) {
    this.username = (<HTMLInputElement>event.target).value;
    if (this.username != '') {
      this.allowclick = true;
    }
  }

  onResetUser() {
    this.username = '';
    
  }

}
