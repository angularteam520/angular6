import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [`
    .futurelog {
        color: white;
    }
  `]
})
export class AppComponent {
  togglePassword = false;
  log = [];

  onToggle() {
    this.togglePassword = this.togglePassword ? false : true;
    this.log.push(this.log.length + 1);
  }
}
